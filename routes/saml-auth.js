const express = require('express');
const router = express.Router();

router.init = function(passport) {
    router.get('/login', passport.authenticate('saml',
    {
        failureRedirect: '/',
        failureFlash: true
    }))

    router.post('/callback', passport.authenticate('saml'),(req, res) => {
        res.redirect(req.body.RelayState)
    })
    return router
}
module.exports = router.init;