exports.build = function(attrs) {
  var profile = {
        uid : attrs['id'] || 'anonymous',
        mail : attrs['mail'],
        dn : attrs['dn'],
        gn : attrs['gn'],
        sn : attrs['sn']
  }
  return profile
}
