module.exports = {
    entityID: 'urn:xander:appnexus:aaim:snewton',
    callbackUrl: 'https://s.apnxs.com/auth/callback',
    signatureAlgorithm: 'sha256',
    identifierFormat: 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    acceptedClockSkewMs: 5000, // 5 seconds
    testshib: {
      signonEndpoint: 'https://idp.testshib.org/idp/profile/SAML2/Redirect/SSO',
      publicKey: 'testshib.crt'
    },
    adfs: {
	signonEndpoint: 'https://adfs.test.corp.appnexus.com/adfs/ls/',
	publicKey: 'adfs.crt'
    },
    aaimIdp: {
        signonEndpoint: 'https://2941.snewton.user.lax1.adnexus.net/idp/profile/SAML2/Redirect/SSO',
	publicKey: 'aaim-idp.crt'
    }
}
