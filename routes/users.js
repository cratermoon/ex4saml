const express = require('express');
const router = express.Router();
const profile = require('../models/profile')
const winston = require('winston')


router.get('/profile', function(req, res) {
  if (req.isAuthenticated()) {
    winston.debug('===== profile >>>>> ' + JSON.stringify(req.user))
    res.render('profile', {
      title: 'SAML Profile for ' + req.user.dn,
      profile:  profile.build(
        req.user
      )
    })
  } else {
    res.render('index',
      {
        title: 'User Not Authenticated',
      });
  }
});

module.exports = router;
