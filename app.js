const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const session = require('express-session');
const winston = require('winston')

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const healthRouter = require('./routes/healthcheck');
const samlAuthRouter = require('./routes/saml-auth');

//
winston.add(new winston.transports.Console({ level: 'info' }))
var e2sMiddleware = require('./middlewares/shib_headers')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
if (process.env.SESSION != "NO") {
  app.use(session(
    {
      name: "apnxssso",
      resave: true,
      saveUninitialized: true,
      secret: 'mUjmG666Q8K6a5%4Y7ga6*67',
    }));
}
app.use(passport.initialize());
app.use(passport.session());
var authConfig = require('./config/auth')(passport);

app.use(e2sMiddleware.extractHeaders);


app.get('/metadata.xml', function (req, res) {
  res.contentType('application/xml')
  res.send(authConfig.strategy.generateServiceProviderMetadata(authConfig.encryptionCert, authConfig.signingCert))
})

app.use('/auth', samlAuthRouter(passport))
app.use('/', indexRouter);
app.use('/health', healthRouter);
app.use('/user', function (req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }
  res.redirect('../auth/login?RelayState=..'+req.originalUrl)
}
)
app.use('/user', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
