/*
  Extract shibboleth-specific variables from the session
*/
exports.build = function(session) {
  var saml_vars = {
        appID : session['shib-application-id'],
        sessionID : session['shib-session-id'],
        authInstant : session['shib-authentication-instant'],
        authMethod : session['shib-authentication-method'],
        authnContextClass : session['shib-authncontext-class'],
        authnContextDecl : session['shib-authncontext-decl'] || 'Unknown',
        handler : session['shib-handler']
  }
  return saml_vars;
}
