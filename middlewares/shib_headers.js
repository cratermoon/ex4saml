var profile = require('../models/profile')
var saml_context = require('../models/saml_context')

exports.extractHeaders = function(req, res, next) {
  const  headers  = req.headers;

  var environment = ''
  Object.keys(headers).forEach(function(key) {
        var val = headers[key];
        environment = environment + key + ': ' + val + '\n';

  });
  req.e4s_profile = profile.build(headers);
  req.e4s_saml_context = saml_context.build(headers);
  req.e4s_env = environment
  next()
}
