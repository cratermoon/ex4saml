var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.contentType('application/json')
  res.send('{ "status": "ok" }')
});
module.exports = router;
