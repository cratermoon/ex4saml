var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'AAIM SAML Welcome Demo', profile:  req.e4s_profile, env: req.e4s_env, saml: req.e4s_saml_context});

});

module.exports = router;
