const winston = require('winston')
const fs = require('fs')
const SamlStrategy = require('passport-saml').Strategy;
const settings = require('./saml')

var samlStrategy


function init (passport) {
    const spname = process.env.SPNAME
    const myKey = fs.readFileSync('./saml.key', 'utf-8');
    winston.info(settings[spname].signonEndpoint)
    winston.info(settings.entityID)
    const idpCert = fs.readFileSync('./'+ settings[spname].publicKey).toString().replace(/-----.* CERTIFICATE-----/g,'').replace(/\r?\n|\r/g,'')
    samlStrategy = new SamlStrategy(
    {
      //host: 's.apnxs.com',
      //path: '/sp/auth/callback',
      callbackUrl: settings.callbackUrl,
      signatureAlgorithm: settings.signatureAlgorithm,
      entryPoint: settings[spname].signonEndpoint,
      issuer: settings.entityID,
      identifierFormat: settings.identifierFormat,
      privateCert: myKey.toString(), // yes, the key, not the cert
      decryptionPvk: myKey.toString(),
      acceptedClockSkewMs: 5000, // 5 seconds
      cert: idpCert
    },
    function (profile, done) {
      return done(null,
        {
          id: profile.id,
          mail: profile.mail,
          //dn: profile['urn:oid:2.16.840.1.113730.3.1.241'],
          dn: profile['http://schemas.xmlsoap.org/claims/CommonName'],
          //gn: profile['urn:oid:2.5.4.42'],
          gn: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'],
          //sn: profile['urn:oid:2.5.4.4']
          sn: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname']
        });
    })
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });
  passport.use(samlStrategy);
  var publicCert = fs.readFileSync('./saml.crt').toString()
  return {
      strategy: samlStrategy,
      encryptionCert: publicCert,
      signingCert: publicCert,
  }
}
module.exports = init
